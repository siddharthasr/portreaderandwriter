/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package portdatareaderwriter;

/**
 *
 * @author LENOVO
 */
public class PortDataReaderWriterMain 
{
        public static void main(String[] args) 
    {
        System.out.println("Input arguments to PortDataReaderWriterMain :\n\n") ;
        
        for(int loop1 = 0 ; loop1 < args.length ; loop1++)
        {
            System.out.println("arg[" + loop1 + "] : " + args[loop1].toString() + "\n") ;
        }
        
        try
        {
            PortDataReaderWriter actualWorkingClass = new PortDataReaderWriter() ;
            actualWorkingClass.doTheWork(args) ;
        }
        catch (Exception e)
        {
            System.out.println("Exception caught in main :\n") ;
            e.printStackTrace(System.out) ;
        }
    }
}
