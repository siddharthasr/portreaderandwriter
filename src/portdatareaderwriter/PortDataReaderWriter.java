/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package portdatareaderwriter;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.Iterator;
import java.util.Vector;
import ssrcommon.SSRCommonJavaFunctionalityLibrary;

/**
 *
 * @author LENOVO
 */
public class PortDataReaderWriter 
{
    public enum OPERATION {NONE, DIRECT_TEXT_TO_PORT} ;
    
    protected PortDataReaderWriter.OPERATION m_operationToPerform ;
    protected String m_IPAddressToWriteTo ;
    protected Integer m_portToWriteTo ;
    protected String m_directTextToPort ;
    protected Boolean m_readAndDisplayStringResponse ;
    
    
    public PortDataReaderWriter()
    {
        this.m_operationToPerform = PortDataReaderWriter.OPERATION.NONE ;
    }

    public void doTheWork(String[] i_paramsToWorkWith) throws Exception
    {
        this.consumeParameters(i_paramsToWorkWith) ;
        this.executeParameters() ;
    }
    
    protected void consumeParameters(String[] i_paramtersToConsume) throws Exception
    {
        Vector<String> commandArguments = this.cleanAndConvertParamsToVector(
                i_paramtersToConsume) ;
        Iterator<String> paramsIterator = commandArguments.iterator() ;
        while(paramsIterator.hasNext())
        {
            String switchOption = paramsIterator.next() ;
            if(true == switchOption.equals("-i"))
            {
                if(false == paramsIterator.hasNext())
                {
                    throw new Exception("IP address not given after option '-i'") ;
                }
                this.m_IPAddressToWriteTo = paramsIterator.next() ;
            }
            else if(true == switchOption.equals("-p"))
            {
                if(false == paramsIterator.hasNext())
                {
                    throw new Exception("Port number not given after option '-p'") ;
                }
                this.m_portToWriteTo = Integer.parseInt(paramsIterator.next()) ;
            }
            else if(true == switchOption.equals("-t"))
            {
                if(false == paramsIterator.hasNext())
                {
                    throw new Exception("Text message not given after option '-t'") ;
                }
                this.m_directTextToPort = paramsIterator.next() ;
                this.m_operationToPerform = PortDataReaderWriter.OPERATION.DIRECT_TEXT_TO_PORT ;
            }
            else if(true == switchOption.equals("-so"))
            {
                this.m_readAndDisplayStringResponse = true ;
            }
        }
    }
    
    protected void executeParameters() throws Exception
    {
        switch(this.m_operationToPerform)
        {
            case NONE : 
            {
                System.out.println("No operation was specified.\nExiting gracefully.") ;
                break ;
            }
            case DIRECT_TEXT_TO_PORT :
            {
                this.performDirectTextToPort() ;
                break ;
            }
            default :
            {
                throw new Exception("Unknown operation requested") ;
            }
        }
    }
    
    //************************************************************************
    protected void performDirectTextToPort() throws Exception
    {
        if(null == this.m_IPAddressToWriteTo || true == this.m_IPAddressToWriteTo.equals(""))
        {
            throw new Exception("IP address not provided for direct text to port") ;
        }
        if((null == this.m_portToWriteTo || 0 == this.m_portToWriteTo))
        {
            throw new Exception("port number not provided to direct text to port") ;
        }
        Socket remoteSocket = new Socket() ;
        remoteSocket.connect(new InetSocketAddress(this.m_IPAddressToWriteTo, 
                this.m_portToWriteTo), 10000) ;
        ObjectOutputStream objectStreamToUse ;
        objectStreamToUse = new ObjectOutputStream(
                remoteSocket.getOutputStream());
        objectStreamToUse.writeObject(this.m_directTextToPort) ;
        
        System.out.println("\nText written to remote port : " + this.m_directTextToPort) ;
        
        if(true == this.m_readAndDisplayStringResponse)
        {
            ObjectInputStream responseInputStream = new ObjectInputStream(
                    remoteSocket.getInputStream()) ;
            String response = (String)responseInputStream.readObject() ;
            System.out.println("\nResponse received : " + response) ;
        }
    }
    //************************************************************************
    
    
    protected Vector<String> cleanAndConvertParamsToVector(String[] i_paramsToConvert)
            throws Exception
    {
        Vector<String> toReturn = new Vector<String>() ;
        for(int loop1 = 0 ; loop1 < i_paramsToConvert.length ; loop1++)
        {
            if(false == i_paramsToConvert[loop1].equals("")
                    && false == i_paramsToConvert[loop1].equals("\t"))
            {
                toReturn.add(i_paramsToConvert[loop1]) ;
            }
        }
        return toReturn ;
    }
    
}
